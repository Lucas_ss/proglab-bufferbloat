#!/usr/bin/python

# Copyright 2013-present Barefoot Networks, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from scapy.all import sniff, sendp
from scapy.all import Packet
from scapy.all import ShortField, IntField, LongField, BitField

import sys
import struct

def handle_pkt(pkt):
    pkt = str(pkt)
    preamble = pkt[:8]
    preamble_exp = "\x00" * 8
    if preamble == preamble_exp:
        print "Recived RNS Packet"
        rot_len = ord(pkt[8])
        rotulo = pkt[9:8+rot_len]
        msg = pkt[9+rot_len:]
        print "rot_len: " + str(rot_len)
        print "rotulo: " + rotulo

    else:
        print "Recived Ipv4 Packet"


    print "desmenbring packet:"
    for idx in xrange(len(pkt)):
        if preamble == preamble_exp and idx < (9+rot_len):
            print idx," RNS_Byte: ",ord(pkt[idx])
        else:
            if preamble == preamble_exp:
                print (idx -(9+rot_len))," IPv4_Byte: ",ord(pkt[idx])
            else:
                print idx," IPv4_Byte: ",ord(pkt[idx])
    #print "Rotulo: " + pkt[13:(13+rot_len)]
    sys.stdout.flush()

def main():
    sniff(iface = "eth0",
          prn = lambda x: handle_pkt(x))

if __name__ == '__main__':
    main()
