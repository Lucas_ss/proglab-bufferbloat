/*
Copyright 2013-present Barefoot Networks, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/




// table_add route_pkt set_priority $rotulo => $priority
header_type intrinsic_metadata_t {
    fields {
        ingress_global_timestamp : 48;
        lf_field_list : 8;
        mcast_grp : 16;
        egress_rid : 16;
        resubmit_flag : 8;
        recirculate_flag : 8;
        priority : 3;
    }
}
metadata intrinsic_metadata_t intrinsic_metadata;

header_type local_t {
    fields {
        rot: 512;
        logical_port: 4;
    }
}
metadata local_t local;


header_type pre_t{
    fields{
        preamble: 64;
    }
}

header pre_t pre;

header_type rns_t {
    fields {
        rot_len: 8;
        rotulo: *;
    }
     length: (rot_len + 1);
     max_length: 256;
}

header rns_t rns;

parser start {
    extract(pre);
    return select(latest.preamble){
        0: parse_rns;
        default: ingress;
    }
}

parser parse_rns{
    extract(rns);
    return ingress;
}

action _drop() {
    drop();
}
action set_priority(){
    modify_field(local.logical_port,(rns.rotulo%7));
    modify_field(intrinsic_metadata.priority, (local.logical_port % 2));
    modify_field(standard_metadata.egress_spec,((local.logical_port/2)+1));
}

action set_local(){
    modify_field(local.rot, rns.rotulo);
    set_priority();
}

table empty{
    reads {
        rns: valid;
    }
    actions {
        set_local;
    }
    size: 1;
}

control ingress {
    apply(empty);
}

control egress {
    // leave empty
}
