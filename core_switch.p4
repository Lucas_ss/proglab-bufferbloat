/*
Copyright 2013-present Barefoot Networks, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/




// table_add route_pkt set_priority $rotulo => $priority
 header_type intrinsic_metadata_t {
    fields {
        ingress_global_timestamp : 48;
        lf_field_list : 8;
        mcast_grp : 16;
        egress_rid : 16;
        resubmit_flag : 8;
        recirculate_flag : 8;
        priority : 3;
    }
}
metadata intrinsic_metadata_t intrinsic_metadata;

header_type rns_t {
    fields {
        preamble: 64;
        rot_len: 8;
        rotulo: *;
    }
    length: (rot_len + 9);
    max_length: 256;
}

header rns_t rns;

parser start {
    extract(rns);
    return ingress;
}

action _drop() {
    drop();
}
action set_priority(priority){
    modify_field(intrinsic_metadata.priority, priority);
    modify_field(standard_metadata.egress_spec,(rns.rotulo%2));
}

action forward(){
    modify_field(intrinsic_metadata.priority, 7);
    modify_field(standard_metadata.egress_spec, (rns.rotulo%2));
}

table table_priority {
    reads {
        rns: valid;
    }
    actions {
        set_priority;
        forward;
        _drop;
    }
    size: 256;
}

control ingress {
    apply(table_priority);
}

control egress {
    // leave empty
}
