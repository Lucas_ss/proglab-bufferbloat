#coding: utf-8
from mininet.net import Mininet
from mininet.topo import Topo
from mininet.log import setLogLevel, info
from mininet.cli import CLI
from mininet.link import TCLink

from p4_mininet import P4Switch, P4Host

import argparse
from time import sleep
import os
import subprocess
import shutil

import networkx as nx #for dijkstra
import string

COMMANDS_FLAG = True

_THIS_DIR = os.path.dirname(os.path.realpath(__file__))
_THRIFT_BASE_PORT = 22222

parser = argparse.ArgumentParser(description='Mininet demo')
#parser.add_argument('--behavioral-exe', help='Path to behavioral executable',
#                    type=str, action="store", required=True)
#parser.add_argument('--json', help='Path to JSON config file',
#                    type=str, action="store", required=True)
#parser.add_argument('--cli', help='Path to BM CLI',
#                    type=str, action="store", required=True)
parser.add_argument('--mode', choices=['l2', 'l3'], type=str, default='l3')

args = parser.parse_args()

#main_json = "simple_router.json"

class MyTopo(Topo):
    def __init__(self, sw_path, nb_hosts, nb_switches, links, **opts):
        # Initialize topology and default options
        Topo.__init__(self, **opts)

        nb_hosts, nb_switches, links = read_topo()
        create_base_files(nb_hosts, nb_switches, links)
        populate_files()
        generate_jsons("p4_files")

        for i in xrange(nb_switches):
            print "Adding Switch s%d" % (i + 1)
            print "config s%d with json_files/sw_%d.json" %((i + 1),(i + 1))
            switch = self.addSwitch('s%d' % (i + 1),
                    sw_path = sw_path,
                    json_path = "json_files/sw_%d.json" % (i + 1),
                    thrift_port = _THRIFT_BASE_PORT + i,
                    pcap_dump = True,
                    device_id = i)
            print "done!"

        for h in xrange(nb_hosts):
            host = self.addHost('h%d' % (h + 1),
                                ip = "10.0.%d.10/24" % h,
                                mac = '00:04:00:00:00:%02x' %h)

        for a, b in links:
            self.addLink(a, b, bw = 2)


def read_topo():
    nb_hosts = 0
    nb_switches = 0
    links = []
    with open("topo.txt", "r") as f:
        line = f.readline()[:-1]
        w, nb_switches = line.split()
        assert(w == "switches")
        line = f.readline()[:-1]
        w, nb_hosts = line.split()
        assert(w == "hosts")
        for line in f:
            if not f: break
            a, b = line.split()
            links.append( (a, b) )
    return int(nb_hosts), int(nb_switches), links


def ext_euclid(a, b):
    if b == 0 :
            return (a, 1, 0)
    else :
            (d, xx, yy) = ext_euclid(b, a % b)
            x = yy
            y = xx - (a / b) * yy
            return (d, x, y)

def inverse(a, n):
    return ext_euclid(a, n)[1]

def int_to_bytes(val, num_bytes):
    return [(val & (0xff << pos*8)) >> pos*8 for pos in range(num_bytes)]

def Teorema_do_resto(shortest_path,port_list):
    for i in xrange (len(port_list)):
        port_list[i] = (port_list[i] -1) *4


    dict_sw = {}
    with open("host_keys.txt", "r") as f:
        for line in f:
            if not f: break
            a, b = line.split()
            dict_sw[a] = b;
 # dicionario que contem os switches e chaves ex: s1 5
    #for x in dict_sw:
    #    print x + " --- " + dict_sw[x] + "\n"
	#k = int(raw_input())
	k= int(len(shortest_path)-1);
	a = []
	n = []
	N = []
	b = []

    for i in xrange(1, k):
        #a_i, n_i = map(long, raw_input().split())
        a_i = long(port_list[i-1]);
        n_i = long(dict_sw[shortest_path[i]])
        a.append(a_i)
        n.append(n_i)
        print "$i max:", i
    print "chaves locais: ", n
    print "portas de saída: ", a


    n_product = 1
    for i in xrange(0, k-1):
        #print "produto: ",n_product
        #print "chave: ", n[i]
        n_product = n_product * n[i]
    #print "Produto das chaves: ", n_product

    for i in xrange(0, k-1):
        print "#i max:", i
        N_term = n_product / n[i]
        N.append(N_term)
    	#print "N: ", N
		#N_term = 1
		#for j in xrange(0, k-1):
		#        if i != j:
		#        	N_term = N_term * n[j]
		#N.append(N_term)

    for i in xrange(0, k-1):
        print "@i max:",i
        b.append(inverse(N[i], n[i]))
        print "b: ", b



    x = long(0)
    for i in xrange(0, k-1):
        x = x + a[i] * N[i] * b[i]
        x = x % n_product

    #Debugs
    #print "\nN LIST( chaves): "
    #for i in xrange(0,len(n)):
    #    print str(n[i]) + ", "
    #print "\nA LIST ( PORTAS ): "
    #for i in xrange(0,len(a)):
#        print str(a[i]) + ", "

    #print "\nHOSTS + SWITCHES: "
    #for i in xrange(0,k+1):
    #    print str(shortest_path[i]) + ", "
    #print "\nSWITCHES + chaves: "
    #for i in xrange(1,k):
    #    print str(shortest_path[i]) +" - "+ str(dict_sw[shortest_path[i]]) + "\n"

    print "\n\n\n\nRESULTADO DO TEOREMA DO RESTO:\n\n " + str(x)
    rot_l = int_to_bytes(int(x),255)
    rotulo = str()
    flag = False
    for i in reversed(rot_l):
        if (i != 0) or flag :
            flag = True
            rotulo += chr(i)
    print "n_rot: ",x
    print "str_rot", rotulo
    return x, rotulo

def dijkstra(nb_hosts, nb_switches, links, src, dst):

    port_map = {}

    for a, b in links:
        if a not in port_map:
            port_map[a] = {}
        if b not in port_map:
            port_map[b] = {}

        assert(b not in port_map[a])
        assert(a not in port_map[b])
        port_map[a][b] = len(port_map[a]) + 1
        port_map[b][a] = len(port_map[b]) + 1


    G = nx.Graph()
    for a, b in links:
        G.add_edge(a, b)

    shortest_paths = nx.shortest_path(G)
    shortest_path = shortest_paths[src][dst]

    print "path is:", shortest_path

    port_list = []
    first = shortest_path[1]
    for h in shortest_path[2:]:
        port_list.append(port_map[first][h])
        first = h

    print "port list is:", port_list

    return shortest_path, port_list

# Funções para manipular a criação dos arquivos p4:
def add_rns_header(h_len,file_name):
    print "Adding header rns_rot_" + str(h_len)
    if h_len%8 != 0:
        h_len += (-1)*(h_len%8 -8)

    rns_header = "header rns_rot_%d_t{\n   bit<%d> rotulo;\n}\n" %((h_len/8),h_len)
    rns_emit = "packet.emit(hdr.rns_rot_%d);" % (h_len/8)
    rns_type_to_name = "rns_rot_%d_t rns_rot_%d;\n\t$rns_type+name" % (h_len/8,h_len/8)
    select_rot_len = "\n\t\thdr.rns_rot_%d.setValid();\t\thdr.rns_rot_%d.rotulo = (bit<%d>)rotulo;\n\t$select_rot_len"  % (h_len/8,h_len/8,h_len)
    set_rns_invalid = "hdr.rns_rot_%d.setInvalid();\n\t\t$set_rns_invalid" % (h_len/8)
    rns_name = "rns_rot_%d" % (h_len/8)

    # \n\t\t\thdr.rns_rot_%d.setValid();

    print "##opening ", file_name
    f = open(file_name,'r')
    data  = f.read()
    f.close()
    if data.find(rns_header) == -1:
        rns_header += "$rns_header"
        data = data.replace("$rns_header",rns_header)
        data = data.replace("$rns_type+name",rns_type_to_name)
        data = data.replace("$select_rot_len",select_rot_len)
        data = data.replace("$rns_emit",rns_emit)
        data = data.replace("$set_rns_invalid",set_rns_invalid)
        data = data.replace("$rns_name",rns_name)


        #print data
        f = open(file_name,'w+')
        f.write(data)
        f.close()
        print "done !"


def add_ipv4_lpm_to_rns_entry(file_name,ip,dmac,port,rot_len,rotulo):
    new_entry = "table_add ipv4_lpm ipv4_forward"+str(ip)+"/32 => "+str(dmac)+" "+ str(port)
    new_entry += " "+str(rot_len)+ " " + str(rotulo)
    f = open(file_name,'r')
    data  = f.read()
    f.close()
    if data.find(new_entry) == -1:
        new_entry += "\n$ipv4_lpm"
        data = data.replace("$ipv4_lpm",new_entry)
        f = open(file_name,'w+')
        f.write(data)
        f.close()

def add_rns_to_ipv4_entry(file_name,ip,dmac,port,rot_len,rotulo):
    new_entry = "table_add rns_ipv4 rns_ipv4_forward"+str(ip)+"/32 => "+str(dmac)+" "+ str(port)
    f = open(file_name,'r')
    data  = f.read()
    f.close()
    if data.find(new_entry) == -1:
        new_entry += "\n$rns_ipv4"
        data = data.replace("$rns_ipv4",new_entry)
        f = open(file_name,'w+')
        f.write(data)
        f.close()

def add_rns_to_ipv4_key(file_name,key):
    f = open(file_name,'r')
    data = f.read()
    f.close()
    f = open(file_name,'w')
    data = data.replace("$key",key)
    f.write(data)
    f.close()


def is_edge(sw,links):
    for i in links:
        if (sw == i[0] and i[1][0] == 'h') or (sw == i[1] and i[0][0] == 'h'):
            return True
    return False

def create_base_files(nb_hosts,nb_switches,links):
    #criar pasta e preencher tabelas
    current_dir = os.getcwd()
    if os.path.exists(current_dir + "/sw_commands"):
        global COMMANDS_FLAG
        COMMANDS_FLAG = False
        #shutil.rmtree(current_dir + "/sw_commands")
    sleep(1)
    if COMMANDS_FLAG:
        os.makedirs(current_dir + "/sw_commands")

        for n in xrange(nb_switches):
            f = open(current_dir + "/base_commands.txt",'r')
            data = f.read()
            f.close()
            f = open(current_dir + "/sw_commands/commands_s%d.txt" %(n+1),'w+')
            f.write(data)
            f.close

    #criar pasta para os arquivos p4 dos switches de borda
    if os.path.exists(current_dir + "/p4_files"):
        shutil.rmtree(current_dir + "/p4_files")
    sleep(1)
    os.makedirs(current_dir + "/p4_files")
    for n in xrange(nb_switches):
        if is_edge(str("s" + str(n+1)),links):
            f = open(current_dir + "/edge_router_16_base.p4",'r')
            data = f.read()
            f.close()
            f = open(current_dir + "/p4_files/sw_%d.p4" %(n+1),'w+')
            f.write(data)
            f.close

    #criar pasta para os arquivos json dos switches
    if os.path.exists(current_dir + "/json_files"):
        shutil.rmtree(current_dir + "/json_files")
    sleep(1)
    os.makedirs(current_dir + "/json_files")


def erase_from_file(file_name,texto):
    f = open(file_name,'r')
    data = f.read()
    f.close()
    data = data.replace(texto,'')
    f = open(file_name,'w')
    f.write(data)
    f.close()

def populate_files():
    nb_hosts, nb_switches, links = read_topo()
    #Preencher tabelas
    maior_len = 0
    maior_rot = ""
    for h1 in xrange(nb_hosts): #hosts
        for h2 in xrange(nb_hosts): #host a host
            if h1 != h2 :
                shortest_path, port_list = dijkstra(nb_hosts, nb_switches, links,
                    'h%d' % (h1 + 1),'h%d' % (h2 + 1))
                rotulo_n, rotulo_str = Teorema_do_resto(shortest_path, port_list)
                if maior_len < len(rotulo_str):
                    maior_len = len(rotulo_str)
                    maior_rot = rotulo_str
                    print "CHANGE %d TO %d" % (maior_len,len(rotulo_str))

    for h1 in xrange(nb_hosts): #hosts
        #h1 = net.get('h%d' % (n + 1))
        for h2 in xrange(nb_hosts): #host a host
            #h2 = net.get('h%d' % (m + 1))
            if h1 != h2 :
                shortest_path, port_list = dijkstra(nb_hosts, nb_switches, links,
                    'h%d' % (h1 + 1),'h%d' % (h2 + 1))
                dict_sw = {}

                for l in xrange(1,len(shortest_path)-1):
                    dict_sw[shortest_path[l]] = str(port_list[l-1])

                rotulo_n, rotulo_str = Teorema_do_resto(shortest_path, port_list)
                # if maior_len < len(rotulo_str):
                #     maior_len = len(rotulo_str)
                #     maior_rot = rotulo_str
                #     print "CHANGE %d TO %d" % (maior_len,len(rotulo_str))


                print "@@@@@@\n@@@@\n\n\n@@@@@",rotulo_n

                #New, for the edge switches
                if COMMANDS_FLAG:
                    for sw in shortest_path:
                        if str(sw)[0] == 's' and is_edge(str(sw),links):
                            commands_name = "sw_commands/commands_%s.txt" % sw
                            p4_name = "p4_files/sw_%s.p4" % sw[1]
                            #f = open(current_dir + "/sw_edge_p4/sw_%s.p4" %sw[1],'rw')
                            add_ipv4_lpm_to_rns_entry(commands_name," 10.0.%d.10" % h2,'00:04:00:00:00:%02x' %h2,dict_sw[sw],maior_len,rotulo_n)
                            add_rns_to_ipv4_entry(commands_name," 10.0.%d.10" % h2,'00:04:00:00:00:%02x' %h2,dict_sw[sw],len(rotulo_str),rotulo_n)

                        elif str(sw)[0] == 's':
                            commands_name = "sw_commands/commands_%s.txt" % sw
                            f = open('core_commands.txt','r')
                            data = f.read()
                            f.close()
                            f = open(commands_name,'w')
                            f.write(data)
                            f.close()

                    #print "\n\n\n\n\n\n\n@@@@@@@@@\nTOP LEN: %d \n\@@@@@n\n\n\n\n\n" % maior_len

    for n in xrange(nb_switches):
        if is_edge("s%d" % (n+1),links):
            p4_name = "p4_files/sw_%d.p4" % (n+1)
            add_rns_header(maior_len*8,p4_name)


    for n in xrange(nb_switches):
        if is_edge("s%d" %(n+1),links):
            erase_from_file("sw_commands/commands_s%d.txt" % (n+1),"\n$ipv4_lpm")
            erase_from_file("sw_commands/commands_s%d.txt" % (n+1),"\n$rns_ipv4")
            erase_from_file("p4_files/sw_%d.p4" % (n+1),"$rns_type+name")
            erase_from_file("p4_files/sw_%d.p4" % (n+1),"$rns_header")
            erase_from_file("p4_files/sw_%d.p4" % (n+1),"$select_rot_len")
            erase_from_file("p4_files/sw_%d.p4" % (n+1),"$rns_emit")
            erase_from_file("p4_files/sw_%d.p4" % (n+1),"$set_rns_invalid")



def generate_jsons(p4_files_path):
    nb_hosts, nb_switches, links = read_topo()
    # arquivo = []
    # for _, _, f in os.walk(p4_files_path):
    #     arquivo = f
    # print arquivo

    # for f in arquivo:
    #     os.system("sudo p4c-bm2-ss %s/sw_1.p4 -o json_files/%s.json" % (p4_files_path,f[:-3]))

    keys = read_host_keys()

    for sw in xrange(nb_switches):
        if is_edge("s%d" % (sw+1),links):
            os.system("sudo p4c-bm2-ss %s/sw_%s.p4 -o json_files/sw_%s.json" % (p4_files_path,sw+1,sw+1))
        else:
            create_core_json("json_files/sw_%s.json" % (sw+1),keys["s%d"%(sw+1)])


def create_core_json(file_name,key):
    f = open("tableless.json",'r')
    data = f.read()
    f.close()
    f = open(file_name,'w')
    f.write(data.replace("$chave",hex(key)))
    f.close()


def read_host_keys():
    keys = {}
    with open("host_keys.txt", "r") as f:
        for line in f:
            if not f: break
            sw, key = line.split()
            keys[sw] = int(key)
    return keys

def main():
    mode = args.mode
    behavioral_exe = '/home/p4/bmv2/targets/simple_switch/simple_switch'
    cli = '/home/p4/bmv2/tools/runtime_CLI.py'

    nb_hosts, nb_switches, links = read_topo()

    topo = MyTopo(behavioral_exe,
                  nb_hosts, nb_switches, links)

    net = Mininet(topo = topo,
                  host = P4Host,
                  switch = P4Switch,
                  link = TCLink,
                  controller = None )
    net.start()


    sw_mac = ["00:aa:bb:00:00:%02x" % n for n in xrange(nb_hosts)]

    sw_addr = ["10.0.%d.1" % n for n in xrange(nb_hosts)]

    print "@@@@@@@@@@@\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n@@@@@@@@@@@@@@@@"


    for n in xrange(nb_hosts): # ARP
        h = net.get('h%d' % (n + 1))
        if mode == "l2":
            h.setDefaultRoute("dev eth0")
        else:
            h.setARP(sw_addr[n], sw_mac[n])
            h.setDefaultRoute("dev eth0 via %s" % sw_addr[n])

    for n in xrange(nb_hosts):
        h = net.get('h%d' % (n + 1))
        h.describe()


    for n in xrange(nb_hosts):
        h = net.get('h%d' % (n + 1))
        for off in ["rx", "tx", "sg"]:
            cmd = "/sbin/ethtool --offload eth0 %s off" % off
            print cmd
            h.cmd(cmd)
        print "disable ipv6"
        h.cmd("sysctl -w net.ipv6.conf.all.disable_ipv6=1")
        h.cmd("sysctl -w net.ipv6.conf.default.disable_ipv6=1")
        h.cmd("sysctl -w net.ipv6.conf.lo.disable_ipv6=1")
        h.cmd("sysctl -w net.ipv4.tcp_congestion_control=reno")
        h.cmd("iptables -I OUTPUT -p icmp --icmp-type destination-unreachable -j DROP")

    sleep(1)
    current_dir = os.getcwd()
    for i in xrange(nb_switches):
        cmd = [cli, "--json","json_files/sw_%d.json" % (i + 1),
               "--thrift-port", str(_THRIFT_BASE_PORT + i)]


        with open(current_dir+"/sw_commands/commands_s%d.txt" %(i+1), "r") as f:
            print " ".join(cmd)
            try:
                output = subprocess.check_output(cmd, stdin = f)
                print output
            except subprocess.CalledProcessError as e:
                print e
                print e.output

    sleep(1)

    print "Ready !"

    CLI( net )
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    main()
