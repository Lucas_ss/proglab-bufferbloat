#coding: utf-8
import networkx as nx

def read_topo():
    nb_hosts = 0
    nb_switches = 0
    links = []
    with open("topo.txt", "r") as f:
        line = f.readline()[:-1]
        w, nb_switches = line.split()
        assert(w == "switches")
        line = f.readline()[:-1]
        w, nb_hosts = line.split()
        assert(w == "hosts")
        for line in f:
            if not f: break
            a, b = line.split()
            links.append( (a, b) )
    return int(nb_hosts), int(nb_switches), links


def dijkstra(nb_hosts, nb_switches, links, src, dst):

    port_map = {}

    for a, b in links:
        if a not in port_map:
            port_map[a] = {}
        if b not in port_map:
            port_map[b] = {}

        assert(b not in port_map[a])
        assert(a not in port_map[b])
        port_map[a][b] = len(port_map[a]) + 1
        port_map[b][a] = len(port_map[b]) + 1


    G = nx.Graph()
    for a, b in links:
        G.add_edge(a, b)

    shortest_paths = nx.shortest_path(G)
    shortest_path = shortest_paths[src][dst]

    print "path is:", shortest_path

    port_list = []
    first = shortest_path[1]
    for h in shortest_path[2:]:
        port_list.append(port_map[first][h])
        first = h

    print "port list is:", port_list

    return shortest_path, port_list

def ext_euclid(a, b):
    if b == 0 :
            return (a, 1, 0)
    else :
            (d, xx, yy) = ext_euclid(b, a % b)
            x = yy
            y = xx - (a / b) * yy
            return (d, x, y)

def inverse(a, n):
    return ext_euclid(a, n)[1]

def int_to_bytes(val, num_bytes):
    return [(val & (0xff << pos*8)) >> pos*8 for pos in range(num_bytes)]

def Teorema_do_resto(shortest_path,port_list):
    dict_sw = {}
    with open("host_keys.txt", "r") as f:
        for line in f:
            if not f: break
            a, b = line.split()
            dict_sw[a] = b; # dicionario que contem os switches e chaves ex: s1 5
    for x in dict_sw:
        print x + " --- " + dict_sw[x] + "\n"

	#k = int(raw_input())
	k= int(len(shortest_path)-1);
	a = []
	n = []
	N = []
	b = []

	for i in xrange(1, k):
		#a_i, n_i = map(long, raw_input().split())
		a_i = long(port_list[i-1]);
		n_i = long(dict_sw[shortest_path[i]])
		a.append(a_i)
		n.append(n_i)

	n_product = reduce(lambda x, y: x * y, n, 1)

	for i in xrange(0, k-1):
		N_term = 1
		for j in xrange(0, k-1):
		        if i != j:
		        	N_term = N_term * n[j]
		N.append(N_term)

	for i in xrange(0, k-1):
		b.append(inverse(N[i], n[i]))

	x = 0
	for i in xrange(0, k-1):
		x = x + a[i] * N[i] * b[i] % n_product

    #Debugs
    print "\nN LIST( chaves):\n"
    for i in xrange(0,len(n)):
        print str(n[i]) + ", "
    print "\nA LIST ( PORTAS ):\n"
    for i in xrange(0,len(a)):
        print str(a[i]) + ", "

    print "\nHOSTS + SWITCHES:\n"
    for i in xrange(0,k+1):
        print str(shortest_path[i]) + ", "
    print "\nSWITCHES + chaves:\n"
    for i in xrange(1,k):
        print str(shortest_path[i]) +" - "+ str(dict_sw[shortest_path[i]]) + "\n"

    print "\n\nRESULTADO DO TEOREMA DO RESTO: " + str(x)
    rot_l = int_to_bytes(int(x),4)
    port_str = str()
    flag = False
    for i in reversed(rot_l):
        if (i != 0) or flag :
            flag = True
            port_str += chr(i)

    print port_str
    return x, port_str

if __name__ == '__main__' :
    n_sw, n_h, links = read_topo()
    for h1 in xrange(n_h): #hosts
        for h2 in xrange(n_h): #host a host
            if h1 != h2 :
                shortest_path, port_list =dijkstra(n_sw, n_h, links,'h%d' % (h1 + 1),'h%d' % (h2 + 1))
                x,y =Teorema_do_resto(shortest_path, port_list)
                print x , " ", y
